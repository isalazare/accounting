# Accounting NodeJS service
## THIS MICROSERVICE USES:
- Express
- Awilix
- Winston

## Getting started
### Build for local development

You have to use the following command to start a development server (nodemon):

```sh
npm install
```

```sh
npm run start:dev
```