module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "no-use-before-define": "off",
    "linebreak-style": 0,
    "indent": ["error", 2]
  }
};
