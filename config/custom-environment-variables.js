const customEnvVariables = {
  express: {
    host: 'HOSTNAME',
    port: 'PORT',
  },
};

module.exports = customEnvVariables;
