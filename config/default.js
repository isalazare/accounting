require('dotenv').config();

const config = {
  express: {
    host: 'localhost',
    port: '3000',
  },
  logger: {
    exitOnError: false,
    transports: {
      console: {
        enabled: true,
        level: 'info',
        timestamp: true,
        prettyPrint: true,
        handleExceptions: true,
        humanReadableUnhandledException: true,
      },
    },
  },
  redshift: {
    user: process.env.USERDB,
    password: process.env.PSWDB,
    database: process.env.REDSHIFTDB,
    port: process.env.PORTDB,
    host: process.env.HOSTDB,
  },
  mongodb: {
    database: process.env.MONGODB_DATABASE,
    password: process.env.MONGODB_PASSWORD,
    port: process.env.MONGODB_PORT,
    server: process.env.MONGODB_SERVER,
    user: process.env.MONGODB_USER,
  },
  services: {
    quickbooks: {
      tokenEndpoint: 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer ',
      api_uri: 'https://sandbox-quickbooks.api.intuit.com/v3/company/',
    },
  },
};

module.exports = config;
