const HttpStatus = require('http-status-codes');

function batchController({ batchService }) {
  return {
    post,
  };

  async function post(req, res) {
    const {
      companyId,
      businessDate,
      realmId,
      accessToken,
    } = req.body;

    try {
      const batchResponse = await batchService.post({
        companyId, businessDate, realmId, accessToken,
      });

      res.json(batchResponse);
    } catch (error) {
      const { statusCode, message } = error;
      const finalStatusCode = statusCode || HttpStatus.INTERNAL_SERVER_ERROR;

      res.status(finalStatusCode).send(message);
    }
  }
}

module.exports = batchController;
