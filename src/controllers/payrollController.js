const HttpStatus = require('http-status-codes');

function payrollController({ payrollService }) {
  return {
    post,
  };

  async function post(req, res) {
    const {
      companyId,
      businessDate,
      realmId,
      accessToken,
    } = req.body;

    try {
      const payrollResponse = await payrollService.post({
        companyId, businessDate, realmId, accessToken,
      });

      res.json(payrollResponse);
    } catch (error) {
      const { statusCode, message } = error;
      const finalStatusCode = statusCode || HttpStatus.INTERNAL_SERVER_ERROR;

      res.status(finalStatusCode).send(message);
    }
  }
}

module.exports = payrollController;
