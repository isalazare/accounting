const HttpStatus = require('http-status-codes');

function syncEmployeeController({ syncEmployeeService }) {
  return {
    post,
  };

  async function post(req, res) {
    const {
      companyId,
      realmId,
      accessToken,
    } = req.body;

    try {
      const employeeResponse = await syncEmployeeService.post({
        companyId, realmId, accessToken,
      });

      res.json(employeeResponse);
    } catch (error) {
      const { statusCode, message } = error;
      const finalStatusCode = statusCode || HttpStatus.INTERNAL_SERVER_ERROR;

      res.status(finalStatusCode).send(message);
    }
  }
}

module.exports = syncEmployeeController;
