const uuidv1 = require('uuid/v1');
const HttpStatus = require('http-status-codes');

function batchService({
  transformDataDomain,
  loaderDomain,
  processDataDomain,
  taskManagerDomain,
  exceptionLogDomain,
}) {
  return {
    post,
  };

  function post({
    companyId, businessDate, realmId, accessToken,
  }) {
    const requestId = uuidv1();

    if (!accessToken) {
      const error = new Error('No access token provided');
      error.statusCode = HttpStatus.BAD_REQUEST;
      throw error;
    }

    callBatch({
      token: accessToken,
      realmId,
      companyId,
      businessDate,
      requestId,
    });

    return {
      requestId,
    };
  }

  async function callBatch({
    token, realmId, companyId, businessDate, requestId,
  }) {
    try {
      const rows = await transformDataDomain.transformSalesReceipt({
        companyId, businessDate, realmId, requestId,
      });
      const processDataPromises = [];

      if (rows) {
        const numberAllowRequest = 10;
        const category = 'Sales Receipt';
        let count = await loaderDomain.getNonProcessData({ realmId, requestId, category });
        let limit = 10;
        while (count > 0) {
          const processDataObject = {
            realmId, token, limit, numberAllowRequest, requestId, category,
          };
          processDataPromises.push(processDataDomain.processData(processDataObject));
          count -= numberAllowRequest;
          limit += numberAllowRequest;
        }
      }

      await Promise.all(processDataPromises);
      await taskManagerDomain.endTaskManager({ realmId, requestId });
      return true;
    } catch (err) {
      const error = `Error callBatch: ${err}`;
      exceptionLogDomain.saveExceptionLog({
        realmId, requestId, error,
      });
    }

    return false;
  }
}

module.exports = batchService;
