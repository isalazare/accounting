const uuidv1 = require('uuid/v1');
const HttpStatus = require('http-status-codes');

function syncEmployeeService({
  syncDataDomain,
  exceptionLogDomain,
  taskManagerDomain,
}) {
  return {
    post,
  };

  function post({
    companyId, realmId, accessToken,
  }) {
    const requestId = uuidv1();

    if (!accessToken) {
      const error = new Error('No access token provided');
      error.statusCode = HttpStatus.BAD_REQUEST;
      throw error;
    }

    syncEmployees({
      token: accessToken,
      realmId,
      companyId,
      requestId,
    });

    return {
      requestId,
    };
  }

  async function syncEmployees({
    token, realmId, companyId, requestId,
  }) {
    try {
      await syncDataDomain.syncEmployees({
        realmId, companyId, token, requestId,
      });

      await taskManagerDomain.endTaskManager({ realmId, requestId });

      return true;
    } catch (err) {
      const error = `Error syncEmployees: ${err}`;
      exceptionLogDomain.saveExceptionLog({
        realmId, requestId, error,
      });
    }

    return false;
  }
}

module.exports = syncEmployeeService;
