const express = require('express');

function apiV1Router({ batchController, payrollController, syncEmployeeController }) {
  return express.Router().use('/api/v1', express.Router()
    .post('/batch', batchController.post)
    .post('/payroll', payrollController.post)
    .post('/employee', syncEmployeeController.post));
}

module.exports = apiV1Router;
