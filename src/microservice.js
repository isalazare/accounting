const express = require('express');
const bodyParser = require('body-parser');
const expressWinston = require('express-winston');
const session = require('express-session');
const cors = require('cors');

function microservice({ apiV1Router, logger }) {
  const app = express();

  app.use(cors());
  app.use(bodyParser.json());
  app.use(expressWinston.logger({ winstonInstance: logger }));
  app.use(session({ secret: 'secret', resave: 'false', saveUninitialized: 'false' }));
  app.use(apiV1Router);
  app.use(expressWinston.errorLogger({
    winstonInstance: logger,
    msg: '{{err.message}} {{res.statusCode}} {{req.method}}',
  }));

  return app;
}

module.exports = microservice;
