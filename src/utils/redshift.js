const Redshift = require('node-redshift');

function redshift({ config }) {
  const client = {
    user: config.redshift.user,
    database: config.redshift.database,
    password: config.redshift.password,
    port: config.redshift.port,
    host: config.redshift.host,
  };

  return new Redshift(client);
}

module.exports = redshift;
