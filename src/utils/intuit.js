const doRequest = require('request-promise');

function intuit({ config }) {
  return {
    callBatchAPI,
    callReadDataAPI,
    callCreateEmployeeAPI,
  };

  async function callBatchAPI({
    token, slotRequestId, BatchItemRequest, realmId,
  }) {
    const url = `${config.services.quickbooks.api_uri + realmId}/batch?requestid=${slotRequestId}`;

    const requestObj = {
      method: 'POST',
      url,
      json: true,
      body: {
        BatchItemRequest,
      },
      resolveWithFullResponse: true,
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
      },
    };

    const quickBooksResponse = await doRequest(requestObj);
    const { body, statusCode } = quickBooksResponse;
    return { body, statusCode };
  }

  async function callReadDataAPI({ token, realmId, query }) {
    const url = `${config.services.quickbooks.api_uri + realmId}/query?query=${query}`;
    const requestObj = {
      method: 'POST',
      url,
      json: true,
      resolveWithFullResponse: true,
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
      },
    };

    const quickBooksResponse = await doRequest(requestObj);
    const { body } = quickBooksResponse;
    return body;
  }

  async function callCreateEmployeeAPI({ token, realmId, body }) {
    const url = `${config.services.quickbooks.api_uri + realmId}/employee`;
    const requestObj = {
      method: 'POST',
      url,
      body,
      json: true,
      resolveWithFullResponse: true,
      headers: {
        Authorization: `Bearer ${token}`,
        Accept: 'application/json',
      },
    };

    const quickBooksResponse = await doRequest(requestObj);
    return quickBooksResponse;
  }
}

module.exports = intuit;
