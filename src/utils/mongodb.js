function mongodb({ config }) {
  const connectionString = `mongodb://${config.mongodb.user}:${config.mongodb.password}@${config.mongodb.server}:${config.mongodb.port}/${config.mongodb.database}`;
  return connectionString;
}

module.exports = mongodb;
