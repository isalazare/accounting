const mongoose = require('mongoose');

const { Schema } = mongoose;
const exceptionLogSchema = new Schema({
  realmId: {
    type: String,
    required: true,
  },
  requestId: {
    type: String,
    required: true,
  },
  slotRequestId: {
    type: String,
  },
  error: {
    type: String,
    required: true,
  },
  date: {
    type: Date,
    required: true,
  },
});

exceptionLogSchema.methods.toJSON = () => this.toObject();

function exceptionLogModel() {
  return mongoose.model('ExceptionLog', exceptionLogSchema);
}

module.exports = exceptionLogModel;
