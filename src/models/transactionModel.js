const mongoose = require('mongoose');

const { Schema } = mongoose;
const transactionsSchema = new Schema({
  realmId: {
    type: String,
    required: true,
  },
  requestData: {
    type: Object,
    required: true,
  },
  slotRequestId: {
    type: String,
  },
  responseDate: {
    type: Date,
  },
  state: {
    type: Boolean,
    default: false,
  },
  responseData: {
    type: Object,
  },
  category: {
    type: String,
    required: true,
  },
  parentRequestId: {
    type: String,
    required: true,
  },
  xenialId: {
    type: String,
  },
});

transactionsSchema.methods.toJSON = () => this.toObject();

function transactionModel() {
  return mongoose.model('Transactions', transactionsSchema);
}

module.exports = transactionModel;
