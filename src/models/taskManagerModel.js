const mongoose = require('mongoose');

const { Schema } = mongoose;
const taskManagerSchema = new Schema({
  process: {
    type: String,
    required: true,
  },
  state: {
    type: String,
    required: true,
  },
  startDate: {
    type: Date,
    required: true,
  },
  finishDate: {
    type: Date,
  },
  realmId: {
    type: String,
    required: true,
  },
  requestId: {
    type: String,
    required: true,
  },
  slotRequestId: {
    type: String,
  },
});

taskManagerSchema.methods.toJSON = () => this.toObject();

function taskManagerModel() {
  return mongoose.model('TaskManager', taskManagerSchema);
}

module.exports = taskManagerModel;
