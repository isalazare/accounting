const mongoose = require('mongoose');

const { Schema } = mongoose;
const employeeSchema = new Schema({
  xenialExployeeId: {
    type: String,
    required: true,
  },
  qbEmployeeId: {
    type: String,
    required: true,
  },
  firstName: {
    type: String,
  },
  lastName: {
    type: String,
    required: true,
  },
  realmId: {
    type: String,
    required: true,
  },
  creationDate: {
    type: Date,
    required: true,
  },
});

employeeSchema.methods.toJSON = () => this.toObject();

function employeeModel() {
  return mongoose.model('Employee', employeeSchema);
}

module.exports = employeeModel;
