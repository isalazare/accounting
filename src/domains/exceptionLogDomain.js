function exceptionLogDomain({ exceptionLogModel }) {
  return {
    saveExceptionLog,
  };

  function saveExceptionLog({
    realmId, requestId, slotRequestId, error,
  }) {
    const ExceptionLog = exceptionLogModel;
    const log = new ExceptionLog({
      realmId,
      requestId,
      slotRequestId,
      error,
      date: new Date(),
    });

    return log.save();
  }
}

module.exports = exceptionLogDomain;
