const uuidv1 = require('uuid/v1');

function processDataDomain({
  loaderDomain, taskManagerDomain, intuitGateway, exceptionLogDomain,
}) {
  return {
    processData,
  };

  async function processData({
    realmId, token, limit, numberAllowRequest, requestId, category,
  }) {
    const slotRequestId = uuidv1().substring(0, 20);

    const { id: taskManagerId } = await taskManagerDomain.saveTaskManager({
      name: 'Preparing request to QuickBooks',
      realmId,
      requestId,
    });

    try {
      const BatchItemRequest = [];
      const loadedDatatoProcess = await loaderDomain.loaderData({
        realmId, limit, numberAllowRequest, requestId, category,
      });

      if (loadedDatatoProcess) {
        loadedDatatoProcess.forEach((row) => {
          BatchItemRequest.push(row);
        });

        if (taskManagerId) {
          await taskManagerDomain.updateTaskManager({
            id: taskManagerId, requestId, realmId, slotRequestId,
          });
        }

        return intuitGateway.sendDataToQuickBooks({
          token, realmId, requestId, slotRequestId, BatchItemRequest,
        });
      }
    } catch (err) {
      const error = `Error processData: ${err}`;
      exceptionLogDomain.saveExceptionLog({
        realmId, requestId, slotRequestId, error,
      });

      if (taskManagerId) {
        await taskManagerDomain.updateCompletedWithErrorsTaskManager({
          id: taskManagerId, requestId, realmId, slotRequestId,
        });
      }
    }

    return false;
  }
}

module.exports = processDataDomain;
