function employeeDomain({ employeeModel }) {
  return {
    saveEmployee,
  };

  function saveEmployee({
    xenialExployeeId, qbEmployeeId, realmId, firstName, lastName,
  }) {
    const Employee = employeeModel;
    const newEmployee = new Employee({
      xenialExployeeId,
      qbEmployeeId,
      realmId,
      firstName,
      lastName,
      creationDate: new Date(),
    });

    return newEmployee.save();
  }
}

module.exports = employeeDomain;
