function transformDataDomain({
  taskManagerDomain, transactionDomain, exceptionLogDomain, redshiftGateway, redshiftQueries,
}) {
  return {
    transformSalesReceipt,
    transformPayRoll,
  };

  async function transformSalesReceipt({
    companyId, businessDate, requestId, realmId,
  }) {
    let { id: taskManagerId } = '';

    try {
      const salesReceiptData = await redshiftGateway.getDataFromRedShift({
        realmId,
        requestId,
        query: redshiftQueries.salesReceiptQuery({ companyId, businessDate }),
      });

      taskManagerId = await taskManagerDomain.saveTaskManager({ name: 'Transforming data from RedShift to QuickBooks Json', realmId, requestId });
      const orderNumbers = [];
      const saveTransacionPromises = [];

      salesReceiptData.rows.forEach((element) => {
        const index = orderNumbers.findIndex(order => element.order_id === order);
        if (index === -1) {
          orderNumbers.push(element.order_id);
        }
      });

      const orders = groupBy(salesReceiptData.rows, row => row.order_id);

      orderNumbers.forEach((orderNumber) => {
        let count = 1;
        const lines = [];
        let taxAmount = 0;
        let totalAmount = 0;
        let discountAmount = 0;
        let xenialId = '';

        orders.get(orderNumber).forEach((order) => {
          if (order.items__product_id) {
            const line = {
              LineNum: count,
              Description: order.items__name,
              Amount: order.amount,
              DetailType: 'SalesItemLineDetail',
              SalesItemLineDetail: {
                ItemRef: {
                  // "value": order.items__product_id,
                  value: '11',
                  name: order.items__name,
                },
                UnitPrice: order.items__unit_price,
                Qty: order.items__quantity,
                TaxCodeRef: {
                  value: 'NON',
                },
              },
            };

            lines.push(line);
            count++;
          }

          if (order.record_type === 'tax' && order.record_sub_type === 'order') {
            taxAmount = order.taxes__amount;
          }

          if (order.record_type === 'order_item') {
            totalAmount = order.total;
          }

          if (order.record_type === 'discount' && order.record_sub_type === 'discount_order_level') {
            discountAmount = order.discount__amount;
          }

          xenialId = order.order_id;
        });

        const salesReceipt = {
          SalesReceipt: {
            Line: lines,
            TotalAmt: totalAmount,
            TxnTaxDetail: {
              TotalTax: taxAmount,
            },
          },
        };

        saveTransacionPromises.push(transactionDomain.saveTransaction({
          requestData: salesReceipt, realmId, category: 'Sales Receipt', parentRequestId: requestId, xenialId,
        }));
      });

      await Promise.all(saveTransacionPromises);

      if (taskManagerId) {
        await taskManagerDomain.updateTaskManager({ id: taskManagerId, requestId, realmId });
      }

      return true;
    } catch (err) {
      const error = `Error transformSalesReceipt: ${err}`;
      exceptionLogDomain.saveExceptionLog({
        realmId, requestId, error,
      });

      if (taskManagerId) {
        await taskManagerDomain.updateCompletedWithErrorsTaskManager({
          id: taskManagerId, requestId, realmId,
        });
      }
    }

    return false;
  }

  function groupBy(list, keyGetter) {
    const map = new Map();
    list.forEach((item) => {
      const key = keyGetter(item);
      const collection = map.get(key);
      if (!collection) {
        map.set(key, [item]);
      } else {
        collection.push(item);
      }
    });
    return map;
  }

  async function transformPayRoll({
    companyId, businessDate, requestId, realmId,
  }) {
    let { id: taskManagerId } = '';

    try {
      const payRollData = await redshiftGateway.getDataFromRedShift({
        realmId,
        requestId,
        query: redshiftQueries.payrollQuery({ companyId, businessDate }),
      });

      taskManagerId = await taskManagerDomain.saveTaskManager({
        name: 'Transforming data from RedShift to QuickBooks Json', realmId, requestId,
      });

      const saveTransacionPromises = payRollData.rows.map((payRollItem) => {
        const timeActivity = {
          TimeActivity: {
            TxnDate: businessDate,
            NameOf: 'Employee',
            EmployeeRef: {
              // value: payRollItem.employeenumber,
              // name: `${payRollItem.firstname} ${payRollItem.lastname}`,
              value: '55',
              name: 'Emily Platt',
            },
            // ItemRef: {
            //   value: payRollItem.jobcode,
            //   name: payRollItem.description,
            // },
            ItemRef: {
              value: '8',
              name: payRollItem.description,
            },
            BillableStatus: 'NotBillable',
            Taxable: false,
            HourlyRate: payRollItem.rate,
            StartTime: payRollItem.clockin,
            EndTime: payRollItem.clockout,
          },
        };

        return transactionDomain.saveTransaction({
          requestData: timeActivity, realmId, category: 'PayRoll', parentRequestId: requestId, xenialId: payRollItem.employeenumber,
        });
      });

      await Promise.all(saveTransacionPromises);

      if (taskManagerId) {
        await taskManagerDomain.updateTaskManager({ id: taskManagerId, requestId, realmId });
      }

      return true;
    } catch (err) {
      const error = `Error transformPayRoll: ${err}`;
      exceptionLogDomain.saveExceptionLog({
        realmId, requestId, error,
      });

      if (taskManagerId) {
        await taskManagerDomain.updateCompletedWithErrorsTaskManager({
          id: taskManagerId, requestId, realmId,
        });
      }
    }
    return false;
  }
}

module.exports = transformDataDomain;
