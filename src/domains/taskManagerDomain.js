function taskManagerDomain({ exceptionLogDomain, taskManagerModel }) {
  return {
    saveTaskManager,
    updateTaskManager,
    endTaskManager,
    endWithErrorsTaskManager,
    updateCompletedWithErrorsTaskManager,
  };

  function saveTaskManager({ name, realmId, requestId }) {
    const TaskManager = taskManagerModel;
    const task = new TaskManager({
      process: name,
      state: 'Running',
      startDate: new Date(),
      realmId,
      requestId,
    });

    return task.save();
  }

  function updateTaskManager({
    id, requestId, realmId, slotRequestId,
  }) {
    const TaskManager = taskManagerModel;
    const update = TaskManager.findByIdAndUpdate(id, {
      state: 'Completed', finishDate: new Date(), requestId, slotRequestId,
    }, { new: true, runValidators: true }, (err) => {
      if (err) {
        const error = `Error updateTaskManager: ${err}`;
        exceptionLogDomain.saveExceptionLog({
          realmId, requestId, slotRequestId, error,
        });
      }
    }).exec();

    return update;
  }

  function updateCompletedWithErrorsTaskManager({
    id, requestId, realmId, slotRequestId,
  }) {
    const TaskManager = taskManagerModel;
    const update = TaskManager.findByIdAndUpdate(id, {
      state: 'Completed With Errors', finishDate: new Date(), requestId, slotRequestId,
    }, { new: true, runValidators: true }, (err) => {
      if (err) {
        const error = `Error updateCompletedWithErrorsTaskManager: ${err}`;
        exceptionLogDomain.saveExceptionLog({
          realmId, requestId, slotRequestId, error,
        });
      }
    }).exec();

    return update;
  }

  function endTaskManager({ realmId, requestId }) {
    const TaskManager = taskManagerModel;
    const task = new TaskManager({
      process: 'End Process',
      state: 'Completed',
      startDate: new Date(),
      finishDate: new Date(),
      realmId,
      requestId,
    });

    return task.save();
  }

  function endWithErrorsTaskManager({ realmId, requestId }) {
    const TaskManager = taskManagerModel;
    const task = new TaskManager({
      process: 'End Process with Errors',
      state: 'Completed',
      startDate: new Date(),
      finishDate: new Date(),
      realmId,
      requestId,
    });

    return task.save();
  }
}

module.exports = taskManagerDomain;
