function transactionDomain({ exceptionLogDomain, transactionModel }) {
  return {
    saveTransaction,
    updateTransaction,
  };

  function saveTransaction({
    requestData, realmId, category, parentRequestId, xenialId,
  }) {
    const Transactions = transactionModel;
    const newTransaction = new Transactions({
      realmId,
      requestData: JSON.stringify(requestData),
      category,
      parentRequestId,
      xenialId,
    });

    return newTransaction.save();
  }

  function updateTransaction({
    id, requestId, slotRequestId, responseData, realmId,
  }) {
    const Transactions = transactionModel;
    const update = Transactions.findByIdAndUpdate(id, {
      state: true, responseDate: new Date(), slotRequestId, responseData,
    }, { new: true, runValidators: true }, (err) => {
      if (err) {
        const error = `Error updateTransaction: ${err}`;
        exceptionLogDomain.saveExceptionLog({
          realmId, requestId, error,
        });
      }
    }).exec();

    return update;
  }
}

module.exports = transactionDomain;
