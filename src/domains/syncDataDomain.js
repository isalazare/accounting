function syncDataDomain({
  intuitGateway,
  redshiftGateway,
  quickBooksQueries,
  exceptionLogDomain,
  redshiftQueries,
  employeeDomain,
  taskManagerDomain,
}) {
  return {
    syncEmployees,
  };

  async function syncEmployees({
    realmId, companyId, token, requestId,
  }) {
    let { id: taskManagerId } = '';

    try {
      const quickBooksEmployees = await intuitGateway.readDataFromQuickBooks({
        token, realmId, requestId, query: quickBooksQueries.employeeQuery(),
      });

      const employeesData = await redshiftGateway.getDataFromRedShift({
        realmId,
        requestId,
        query: redshiftQueries.employeeQuery({ companyId }),
      });

      taskManagerId = await taskManagerDomain.saveTaskManager({
        name: 'Synchronization of Employees',
        realmId,
        requestId,
      });

      /* eslint-disable no-await-in-loop */
      for (let i = 0; i < employeesData.rows.length; i += 1) {
        const employee = employeesData.rows[i];
        const existEmployee = quickBooksEmployees.QueryResponse.Employee.filter(item => employee.firstname === item.GivenName && employee.lastname === item.FamilyName);
        if (!existEmployee[0]) {
          const newEmployee = {
            GivenName: employee.firstname,
            FamilyName: employee.lastname,
            Active: true,
          };

          const createdEmployee = await intuitGateway.createEmployeeInQuickBooks({
            token, realmId, requestId, body: newEmployee,
          });

          if (createdEmployee) {
            employeeDomain.saveEmployee({
              xenialExployeeId: employee.employeeid,
              qbEmployeeId: createdEmployee.Employee.Id,
              realmId,
              firstName: employee.firstname,
              lastName: employee.lastname,
            });
          }
        }
      }
      /* eslint-enable no-await-in-loop */

      if (taskManagerId) {
        await taskManagerDomain.updateTaskManager({
          id: taskManagerId, requestId, realmId,
        });
      }

      return true;
    } catch (err) {
      const error = `Error syncEmployees: ${err}`;
      exceptionLogDomain.saveExceptionLog({
        realmId, requestId, error,
      });

      if (taskManagerId) {
        await taskManagerDomain.updateCompletedWithErrorsTaskManager({
          id: taskManagerId, requestId, realmId,
        });
      }
    }

    return false;
  }
}

module.exports = syncDataDomain;
