function loaderDomain({ exceptionLogDomain, transactionModel }) {
  return {
    loaderData,
    getNonProcessData,
  };

  function loaderData({
    realmId, limit, numberAllowRequest, requestId, category,
  }) {
    const Transactions = transactionModel;
    return Transactions.find({
      realmId, state: false, category, parentRequestId: requestId,
    })
      .skip(limit - numberAllowRequest)
      .limit(numberAllowRequest)
      .exec()
      .then(transactions => transactions.map(transaction => Object.assign({},
        JSON.parse(transaction.requestData), { bId: transaction.id })))
      .catch((err) => {
        const error = `Error loaderData: ${err}`;
        exceptionLogDomain.saveExceptionLog({
          realmId, requestId, error,
        });
      });
  }

  function getNonProcessData({ realmId, requestId, category }) {
    const Transactions = transactionModel;
    return Transactions.countDocuments({
      realmId, state: false, category, parentRequestId: requestId,
    }, (err) => {
      if (err) {
        const error = `Error getNonProcessData: ${err}`;
        exceptionLogDomain.saveExceptionLog({
          realmId, requestId, error,
        });
      }
    }).exec();
  }
}

module.exports = loaderDomain;
