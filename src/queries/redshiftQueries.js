const SQL = require('sql-template-strings');

function redshiftQueries() {
  return {
    salesReceiptQuery,
    payrollQuery,
    employeeQuery,
  };

  function salesReceiptQuery({ companyId, businessDate }) {
    return SQL`SELECT order_id, order_number, record_type, record_sub_type, items__product_id,
            items__name, items__unit_price, items__quantity, taxes__tax_definition_id,
            taxes__name, taxes__amount, total, discount__amount, business_date, amount
            FROM xenial_pos_order_details
            WHERE business_date = ${businessDate}
            AND company_id = ${companyId}
            AND payment_status in ('paid', 'refunded')
            AND  state in ('closed', 'paid')
            AND order_type in ('refund', 'order')
            ORDER BY order_number, record_index`;
  }

  function payrollQuery({ companyId, businessDate }) {
    return SQL`SELECT
          pd.BusinessDate,
          pd.EmployeeID AS EmployeeNumber,
          isNULL(e.FirstName, 'UNKNOWN') AS FirstName,
          isNULL(e.LastName, 'UNKNOWN') AS LastName,
          pd.Rate,
          pd.Jobcodeid AS JobCode,
          pd.Jobcodename AS Description,
          CAST(to_char(pd.BusinessDate, 'YYYYMMDD') AS int) AS PayrollDate,
          pd.ClockIn AS ClockIn,
          pd.ClockOut AS ClockOut,
          pd.Total_Tips,
          pd.regularhours,
          pd.regularhoursrate,
          pd.overtimehours,
          pd.overtimehoursrate,
          pd.totalpay,
          CASE
            WHEN pd.ClockInTypeID = '58f66d457020f950c17c3e5e' THEN '1'
            WHEN pd.ClockInTypeID = '58f66d447020f950c17c3e5c' THEN '3'
          ELSE 0 END as ClockInType,
          CASE
            WHEN pd.ClockOutTypeID = '58f66d467020f950c17c3e5f' THEN '2'
            WHEN pd.ClockOutTypeID = '58f66d447020f950c17c3e5d' THEN '4'
          ELSE 0 END as ClockOutType
          FROM
          (
              SELECT *,
                CAST(LEFT(pd.Payroll_Period, position(' - ' IN pd.Payroll_Period)-1) AS DateTime) AS PayStart,
                CAST(RIGHT(pd.Payroll_Period, position(' - ' IN pd.Payroll_Period)-1) AS DateTime) AS PayEnd
              FROM public.xenial_boh_facts_payrolldetail AS pd
          ) AS pd
          LEFT OUTER JOIN xenial_boh_employee AS e
              ON pd.companyid = e.company_id AND pd.siteid = e.primary_site_id AND pd.employeeid = e.employee_corporate_code
          LEFT OUTER JOIN xenial_portal_sites AS s
              ON pd.companyid = s.company_id AND pd.siteid = s._id
          WHERE pd.companyid = ${companyId}
          AND  pd.businessdate = ${businessDate}
          ORDER BY s.store_number, pd.EmployeeID, pd.BusinessDate`;
  }

  function employeeQuery({ companyId }) {
    return SQL`select employee_id AS employeeId, firstname, lastname
          from xenial_boh_employee
          where company_id = ${companyId}
          and is_active = true`;
  }
}

module.exports = redshiftQueries;
