function quickBooksQueries() {
  return {
    employeeQuery,
  };

  function employeeQuery() {
    return 'SELECT Id, GivenName, FamilyName FROM employee';
  }
}

module.exports = quickBooksQueries;
