function redshiftGateway({
  redshift, taskManagerDomain, exceptionLogDomain,
}) {
  return {
    getDataFromRedShift,
  };

  async function getDataFromRedShift({
    realmId, requestId, query,
  }) {
    const { id: taskManagerId } = await taskManagerDomain.saveTaskManager({ name: 'Getting data from RedShift', realmId, requestId });

    try {
      const queryResult = await redshift.query(query, []);
      if (queryResult) {
        const dataJson = JSON.parse(JSON.stringify(queryResult));
        if (taskManagerId) {
          await taskManagerDomain.updateTaskManager({ id: taskManagerId, requestId, realmId });
        }

        return dataJson;
      }
    } catch (err) {
      const error = `Error getDataFromRedShift: ${err}`;
      exceptionLogDomain.saveExceptionLog({
        realmId, requestId, error,
      });

      if (taskManagerId) {
        await taskManagerDomain.updateCompletedWithErrorsTaskManager({
          id: taskManagerId, requestId, realmId,
        });
      }
    }

    return false;
  }
}

module.exports = redshiftGateway;
