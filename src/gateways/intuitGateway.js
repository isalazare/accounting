function intuitGateway({
  taskManagerDomain, transactionDomain, exceptionLogDomain, intuit,
}) {
  return {
    sendDataToQuickBooks,
    readDataFromQuickBooks,
    createEmployeeInQuickBooks,
  };

  async function sendDataToQuickBooks({
    token, realmId, requestId, slotRequestId, BatchItemRequest,
  }) {
    const { id: taskManagerId } = await taskManagerDomain.saveTaskManager({
      name: 'Sending request to QuickBooks',
      realmId,
      requestId,
    });

    try {
      const { body, statusCode } = await intuit.callBatchAPI({
        token, slotRequestId, BatchItemRequest, realmId,
      });

      if (statusCode !== 200) {
        const error = 'Error sending the request to QuickBooks';
        exceptionLogDomain.saveExceptionLog({
          realmId, requestId, slotRequestId, error,
        });

        if (taskManagerId) {
          await taskManagerDomain.updateCompletedWithErrorsTaskManager({
            id: taskManagerId, requestId, realmId, slotRequestId,
          });
        }
        return false;
      }

      const updateTransactionPromises = body.BatchItemResponse.map(batchResponse => transactionDomain.updateTransaction({
        id: batchResponse.bId, requestId, slotRequestId, responseData: batchResponse, realmId,
      }));

      await Promise.all(updateTransactionPromises);

      if (taskManagerId) {
        await taskManagerDomain.updateTaskManager({
          id: taskManagerId, requestId, realmId, slotRequestId,
        });
      }

      return true;
    } catch (err) {
      const error = `Error sendDataToQuickBooks: ${err}`;
      exceptionLogDomain.saveExceptionLog({
        realmId, requestId, slotRequestId, error,
      });

      if (taskManagerId) {
        await taskManagerDomain.updateCompletedWithErrorsTaskManager({
          id: taskManagerId, requestId, realmId, slotRequestId,
        });
      }
    }
    return false;
  }

  async function readDataFromQuickBooks({
    token, realmId, requestId, query,
  }) {
    const { id: taskManagerId } = await taskManagerDomain.saveTaskManager({
      name: 'Getting data from QuickBooks',
      realmId,
      requestId,
    });

    try {
      const result = intuit.callReadDataAPI({ token, realmId, query });
      if (taskManagerId) {
        await taskManagerDomain.updateTaskManager({
          id: taskManagerId, requestId, realmId,
        });
      }
      return result;
    } catch (err) {
      const error = `Error readDataFromQuickBooks: ${err}`;
      exceptionLogDomain.saveExceptionLog({
        realmId, requestId, error,
      });

      if (taskManagerId) {
        await taskManagerDomain.updateCompletedWithErrorsTaskManager({
          id: taskManagerId, requestId, realmId,
        });
      }
    }
    return false;
  }

  async function createEmployeeInQuickBooks({
    token, realmId, requestId, body,
  }) {
    try {
      const response = await intuit.callCreateEmployeeAPI({ token, realmId, body });
      return response.body;
    } catch (err) {
      const error = `Error createEmployeeInQuickBooks: ${err}`;
      exceptionLogDomain.saveExceptionLog({
        realmId, requestId, error,
      });
    }
    return false;
  }
}

module.exports = intuitGateway;
